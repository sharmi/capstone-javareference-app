<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<nav>
    <a href="${pageContext.request.contextPath}/"><img class="logo" src="${pageContext.request.contextPath}/static/img/logo.png" height="30px" width="30px"></a>
    <ul id="menu">
        <li><a href="${pageContext.request.contextPath}/">Home</a></li>
        <li><a href="${pageContext.request.contextPath}/samples">Samples</a></li>
       <li><a href="${pageContext.request.contextPath}/contactus">Contact Us</a></li>
    </ul>
</nav>