package com.capstone.referapp.common;

public class AppConfigConstants {
	
	public static final String CONFIG_DB_DRIVER = "db.driver";
	public static final String CONFIG_DB_PASSWORD = "db.password";
	public static final String CONFIG_DB_URL = "db.url";
	public static final String CONFIG_DB_USERNAME = "db.username";

	public static final String CONFIG_ORM_DIALECT = "hibernate.dialect";
	public static final String CONFIG_ORM_SHOW_SQL = "hibernate.show_sql";
	public static final String CONFIG_ORM_MODEL_PKG = "entitymanager.packages.to.scan";
	
	public static final String CUSTOM_DATE_FORMAT = "app.date.format";;

}
