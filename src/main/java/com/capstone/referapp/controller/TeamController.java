package com.capstone.referapp.controller;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.capstone.referapp.model.Team;
import com.capstone.referapp.service.TeamService;

@Controller
@RequestMapping(value = "/team")
public class TeamController {

	final Logger LOG = LoggerFactory.getLogger(TeamController.class);
	
	@Autowired
	private TeamService teamService;

	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public final String addTeamPage(ModelMap model) {
		model.addAttribute("team", new Team());
		HashMap<String, String> data = new HashMap();
		data.put("a", "A");
		data.put("b", "B");
		data.put("c", "C");
		model.addAttribute("data", data);
		return "team/add-team-form";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public final String addingTeam(@ModelAttribute final Team team, ModelMap model) {
		teamService.addTeam(team);
		String message = "Team was successfully added.";
		model.addAttribute("message", message);
		if (LOG.isDebugEnabled()) {
			LOG.debug("Added team.");
		}
		return "redirect:list";
	}

	@RequestMapping(value = "/list")
	public final String listOfTeams(ModelMap model) {
		List<Team> teams = teamService.getTeams();
		model.addAttribute("teams", teams);
		return "team/list-of-teams";
	}

	@RequestMapping(value = "/edit/{teamId}", method = RequestMethod.GET)
	public final String editTeamPage(@PathVariable final Integer teamId, ModelMap model) {
		Team team = teamService.getTeam(teamId);
		model.addAttribute("team", team);
		return "team/edit-team-form";
	}

	@RequestMapping(value = "/edit/{teamId}", method = RequestMethod.POST)
	public final String edditingTeam(@PathVariable final Integer teamId, @ModelAttribute final Team team, ModelMap model) {
		if(teamId != null) {
			team.setId(new Integer(teamId));
		}
		teamService.updateTeam(team);
		String message = "Team was successfully edited.";
		model.addAttribute("message", message);
		return "home";
	}

	@RequestMapping(value = "/delete/{teamId}", method = RequestMethod.GET)
	public final String deleteTeam(@PathVariable final Integer teamId, ModelMap model) {
		teamService.deleteTeam(teamId);
		String message = "Team was successfully deleted.";
		model.addAttribute("message", message);
		return "home";
	}

}
