package com.capstone.referapp.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.capstone.referapp.service.LocationService;
import com.capstone.referapp.util.model.City;
import com.capstone.referapp.util.model.Country;
import com.capstone.referapp.util.model.State;

@Controller
@RequestMapping(value = "/dict")
public class DictionaryController {

	@Autowired
	LocationService locationService;

	@ResponseBody
	@RequestMapping(value = "/location/countries")
	public List<Country> getCountries() {
		return locationService.getCountries();
	}

	@ResponseBody
	@RequestMapping(value = "/location/states/{country}")
	public List<State> getStates(@PathVariable final String country) {		
		return locationService.getStates(country);
	}

	@ResponseBody
	@RequestMapping(value = "/location/states/{country}/cities/{state}")
	public List<City> getCities(@PathVariable final String country, @PathVariable final String state) {		
		return locationService.getCities(country, state);
	}

}
