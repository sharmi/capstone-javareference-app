package com.capstone.referapp.dao;

import java.util.List;

import com.capstone.referapp.model.Team;

public interface TeamDAO {
	
	void addTeam(Team team);
	void updateTeam(Team team);
	Team getTeam(int teamId);
	void deleteTeam(int teamId);
	List<Team> getTeams();

}
