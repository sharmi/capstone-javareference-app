package com.capstone.referapp.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;

import com.capstone.referapp.util.model.City;
import com.capstone.referapp.util.model.Country;
import com.capstone.referapp.util.model.State;

@Service
public class LocationServiceCacheImpl implements LocationService {

	private HashMap<String, Country> cache = new HashMap<String, Country>();

	public LocationServiceCacheImpl() {
		super();
		
		State tamilnadu = new State("Tamilnadu", "TN");
		tamilnadu.addCity(new City("Chennai", "CHN"));
		tamilnadu.addCity(new City("Coimbatore", "CBE"));
		
		Country india = new Country("India", "IN");
		india.addState(tamilnadu);
		
		

		State illinois = new State("Illinois", "IL");
		illinois.addCity(new City("Chicago", "CHI"));
		illinois.addCity(new City("Naperville", "NPR"));
		
		Country us = new Country("United States", "US");
		us.addState(illinois);
		
		Country uk = new Country("United Kingdom", "UK");
		

		cache.put("IN", india);
		cache.put("US", us);
		cache.put("UK", uk);
	}

	@Override
	public List<Country> getCountries() {
		List<Country> countries = new ArrayList<>();
		for(Country c : cache.values()) {
			countries.add(c);
		}
		
		return countries;
	}

	@Override
	public List<State> getStates(String country) {
		List<State> states = null;
		if(cache.containsKey(country)) {
			states = cache.get(country).getStates();
		}
		return states;
	}

	@Override
	public List<City> getCities(String country, String state) {
		List<City> cities = null;
		if(cache.containsKey(country)) {
			List<State> states = cache.get(country).getStates();
			for(State st : states) {
				if(state.equalsIgnoreCase(st.getAbbrevation())){
					cities = st.getCities();
				}
			}
		}
		return cities;
	}

}
