package com.capstone.referapp.service;

import java.util.List;

import com.capstone.referapp.model.Team;

public interface TeamService {
	
	void addTeam(Team team);
	void updateTeam(Team team);
	Team getTeam(int teamId);
	void deleteTeam(int teamId);
	List<Team> getTeams();

}
