package com.capstone.referapp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.capstone.referapp.dao.TeamDAO;
import com.capstone.referapp.model.Team;

@Service
@Transactional
public class TeamServiceImpl implements TeamService {
	
	@Autowired
	private TeamDAO teamDAO;

	public void addTeam(Team team) {
		teamDAO.addTeam(team);		
	}

	public void updateTeam(Team team) {
		teamDAO.updateTeam(team);
	}

	public Team getTeam(int teamId) {
		return teamDAO.getTeam(teamId);
	}

	public void deleteTeam(int teamId) {
		teamDAO.deleteTeam(teamId);
	}

	public List<Team> getTeams() {
		return teamDAO.getTeams();
	}

}
