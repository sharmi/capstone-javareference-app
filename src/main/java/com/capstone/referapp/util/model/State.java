package com.capstone.referapp.util.model;

import java.util.ArrayList;
import java.util.List;

public class State {

	private String name;
	private String abbrevation;
	private String[] geoLocation;
	private List<City> cities;

	public State(String name, String abbrevation) {
		super();
		this.name = name;
		this.abbrevation = abbrevation;
	}

	public List<City> getCities() {
		return cities;
	}

	public void setCities(List<City> cities) {
		this.cities = cities;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbbrevation() {
		return abbrevation;
	}

	public void setAbbrevation(String abbrevation) {
		this.abbrevation = abbrevation;
	}

	public String[] getGeoLocation() {
		return geoLocation;
	}

	public void setGeoLocation(String[] geoLocation) {
		this.geoLocation = geoLocation;
	}
	public void addCity(City st) {
		if(this.cities == null) {
			this.cities = new ArrayList<City>();
		}
		this.cities.add(st);
	}
}
